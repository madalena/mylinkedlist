package com.mk;

public interface ItemList {
    ListItem getRoot();
    boolean addItem(ListItem item);
    boolean removeItem(ListItem item);
    void print(ListItem item);
}
