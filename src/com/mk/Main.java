package com.mk;

public class Main {

    public static void main(String[] args) {
	// test
        Item newItem1 = new Item("Kleopatra");
        MyLinkedList linkedList = new MyLinkedList(newItem1);
        Item newItem2 = new Item("Alicja");
        linkedList.addItem(newItem2);
        Item newItem3 = new Item("Kleopatra");
        linkedList.addItem(newItem3);

        Item newItem4 = new Item("Kasia");
        linkedList.addItem(newItem4);

        Item newItem5 = new Item("Gerwazy");
        linkedList.addItem(newItem5);

        linkedList.print(linkedList.getRoot());
        linkedList.removeItem(newItem1);
        linkedList.print(linkedList.getRoot());
    }
}
