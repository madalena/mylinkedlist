package com.mk;

public class Item extends ListItem {

    public Item(Object value) {
        super.setValue(value);
    }

    @Override
    ListItem moveToNext() {
        return this.next;
    }

    @Override
    ListItem backToPrevious() {
        return this.previous;
    }

    @Override
    ListItem setPrevious(ListItem previous) {
        this.previous = previous;
        return this.previous;
    }

    @Override
    ListItem setNext(ListItem next) {
        this.next = next;
        return this.next;
    }

    @Override
    int compareTo(ListItem item) {
        if(item != null) {
            return ((String) super.getValue()).compareTo((String) item.getValue()); //rzutowanie na string z object
        }
        else {
            return -1;
        }
    }
}
