package com.mk;

public abstract class ListItem {
    protected ListItem previous = null;
    protected ListItem next = null;
    //private String value;
protected Object value;

    abstract ListItem moveToNext();

    abstract ListItem backToPrevious();

    abstract ListItem setPrevious(ListItem previous);

    abstract ListItem setNext(ListItem next);

    abstract int compareTo(ListItem item);

    //--------------------------------------------------------------

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public ListItem getNext() {
        return next;
    }
}
