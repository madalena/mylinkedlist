package com.mk;

public class MyLinkedList implements ItemList {
    //private ListItem currentItem;
    private ListItem head;

    public MyLinkedList(ListItem head) {
        this.head = head;
    }

    @Override
    public ListItem getRoot() {
        return this.head;
    }

    @Override
    public boolean addItem(ListItem newItem) {
        if(this.head == null) {
            this.head = newItem;
            return true;
        }

        ListItem currItem = this.head;
        while(currItem != null) {
            int comp = (currItem.compareTo(newItem));
            if(comp < 0) {
                //move to next
                if(currItem.moveToNext() != null) {
                    currItem = currItem.moveToNext();
                }
                else {
                    //set in here
                    currItem.setNext(newItem);
                    newItem.setPrevious(currItem);
                    return true;
                }
            } else if( comp > 0) {
                if(currItem.backToPrevious() != null ) {
                    currItem.backToPrevious().setNext(newItem);
                    newItem.setPrevious(currItem.backToPrevious());
                    newItem.setNext(currItem);
                    currItem.setPrevious(newItem);
                }
                else {
                    //head
                    newItem.setNext(this.head);
                    this.head.setPrevious(newItem);
                    this.head = newItem;
                }
                return true;
            }
            else {
                System.out.println("Item " + newItem.getValue() + " is already in our list.");
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean removeItem(ListItem item) {
        if(item != null) {
            System.out.println(item.getValue() + " deleted.");
        }

        ListItem currItem = this.head;
        while(currItem != null) {
            int comp = currItem.compareTo(item);
            if(comp == 0) {
                //deleting
                    if(currItem == this.head) {
                        this.head = currItem.moveToNext();
                    }
                    else {
                        currItem.backToPrevious().setNext(currItem.moveToNext());
                        if(currItem.moveToNext() !=  null) {
                            currItem.moveToNext().setPrevious(currItem.backToPrevious());
                        }
                    }
                return true;
            }
            else if(comp < 0) {
                currItem = currItem.moveToNext();
            }
            else {
                //comp > 0
                //item is not in the list
                return false;
            }
        }
        return false;
    }

    @Override
    public void print(ListItem head) {
        if(head == null)
            System.out.println("List is empty.");
        else {
            while(head != null) {
                System.out.println(head.getValue());
                head = head.moveToNext();
            }
        }
        System.out.println();
    }

    /*public void setNewItem(String newItem) {
        if (this.head == null) {
            Item item = new Item(newItem);
            this.head = item;
            this.currentItem = this.head;
        } else {
            while (this.currentItem != null) {
                if (this.currentItem.getValue().compareTo(newItem) > 0) {
                    Item listItem = new Item(newItem);
                    //this.currentItem.setNext(listItem);
                    //listItem.setPrevious(this.currentItem);
                    //this.currentItem = this.currentItem.moveToNext();
                    if (this.currentItem.backToPrevious() == null) {
                        this.head = listItem;
                        this.head.setNext(this.currentItem);
                        break;
                    } else {
                        listItem.setPrevious(this.currentItem.getNext());
                        this.currentItem.setNext(listItem);
                        //this.currentItem = this.currentItem.moveToNext();
                        break;
                        //current Item pozostaje taki, jaki był
                    }
                } else if (currentItem.getValue().compareTo(newItem) < 0) {
                    Item listItem = new Item(newItem);
                    this.currentItem.setNext(listItem);
                    listItem.setPrevious(this.currentItem);
                    //this.currentItem = this.currentItem.moveToNext();
                    break;

                } else if (this.currentItem.getValue().compareTo(newItem) == 0) {
                    System.out.println("This element is already in the List.");
                    break;
                } else {
                    this.currentItem = this.currentItem.moveToNext();
                }*/
            }
